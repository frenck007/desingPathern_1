package ca.ulaval.glo4003.labodesignpatterns1.singleton.services;

import ca.ulaval.glo4003.labodesignpatterns1.singleton.domain.CourseRepository;
import ca.ulaval.glo4003.labodesignpatterns1.singleton.persistenceB.InMemoryCourseRepository;

public class ServiceLocator {
    
    private static ServiceLocator instance = new ServiceLocator();
    
    private ServiceLocator(){
    
    }
    
    public static ServiceLocator getInstance() {
        return instance;
    }
    
    public InMemoryCourseRepository register (CourseRepository courseRepository, InMemoryCourseRepository inMemoryCourseRepository){
        return inMemoryCourseRepository;
    
    }
    
    public void resolve (CourseRepository courseRepository){
    
    }
}
