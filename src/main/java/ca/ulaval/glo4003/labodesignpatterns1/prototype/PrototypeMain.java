package ca.ulaval.glo4003.labodesignpatterns1.prototype;

import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

public class PrototypeMain {

	public static void main(String[] args) {
		System.out.println("Creating 5 polygons");
		Stopwatch watch = Stopwatch.createStarted();
		Polygon2D polygon1 = new Polygon2D(1, 1);
		Polygon2D polygon2 = new Polygon2D(2, 2);
		Polygon2D polygon3 = new Polygon2D(3, -3);
		Polygon2D polygon4 = new Polygon2D(4, 4);
		Polygon2D polygon5 = new Polygon2D(5, -5);
		
		watch.stop();
		System.out.println(String.format("Created 5 polytons in %s seconds",
				watch.elapsed(TimeUnit.SECONDS)));
		System.out.println(polygon1.toString());
		System.out.println(polygon2.toString());
		System.out.println(polygon3.toString());
		System.out.println(polygon4.toString());
		System.out.println(polygon5.toString());
		
		System.out.println("Creating 5 polygons");
		Stopwatch watch2 = Stopwatch.createStarted();
		
		Polygon2D polygon6 = new Polygon2D(1, 1);
		Polygon2D polygon7 = polygon6.clone(2, 2);
		Polygon2D polygon8 = polygon6.clone(3, -3);
		Polygon2D polygon9 = polygon6.clone(4, 4);
		Polygon2D polygon10 = polygon6.clone(5, -5);
		
		watch2.stop();
		System.out.println(String.format("Created 5 polytons in %s seconds",
				watch2.elapsed(TimeUnit.SECONDS)));
		System.out.println(polygon6.toString());
		System.out.println(polygon7.toString());
		System.out.println(polygon8.toString());
		System.out.println(polygon9.toString());
		System.out.println(polygon10.toString());
	}

}
